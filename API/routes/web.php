<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

    $router->get('affected_location', ['uses' => 'AffectedLocationController@showAllAffected']);
    $router->get('announcement', ['uses' => 'AnnouncementController@showAllAnnouncements']);
    $router->get('announcement/latest', ['uses' => 'AnnouncementController@showLatestAnnouncement']);    
    $router->get('announcement/{id}', ['uses' => 'AnnouncementController@showOneAnnouncement']);
    $router->get('help_request', ['uses' => 'HelpRequestController@showAllHelpRequests']);
    $router->get('help_request/{id}', ['uses' => 'HelpRequestController@showOneHelpRequest']);
    $router->get('missing_person', ['uses' => 'MissingPersonController@showAllMissingPersons']);
    $router->get('missing_person/{id}', ['uses' => 'MissingPersonController@showOneMissingPerson']);
    $router->get('relief_camp_location', ['uses' => 'ReliefCampLocationController@showAllReliefCampLocations']);
    $router->get('relief_camp_location/{id}', ['uses' => 'ReliefCampLocationController@showOneReliefCampLocation']);
    $router->get('rescue_worker/{id}', ['uses' => 'RescueWorkerController@showOneRescueWorker']);
    $router->get('rescue_worker', ['uses' => 'RescueWorkerController@showAllRescueWorkers']);

    $router->post('missing_person', ['uses' => 'MissingPersonController@addMissingPerson']);
    $router->post('help_request', ['uses' => 'HelpRequestController@addHelpRequest']);
    $router->post('help_request/complete',['uses' => 'HelpRequestController@completeHelpRequest']);
    $router->post('affected_location/add/{id}', ['uses' => 'AffectedLocationController@addOneAffected']);
    $router->post('rescue_worker', ['uses' => 'RescueWorkerController@createOrUpdateRescueWorker']);
    $router->post('rescue_worker/assign', ['uses' => 'RescueWorkerController@updateRescueWorkerHelpRequest']);
    //delete requests
    $router->post('affected_location/delete/{id}', ['uses' => 'AffectedLocationController@deleteOneAffected']);
    $router->post('rescue_worker/delete/{phone}', ['uses' => 'RescueWorkerController@deleteRescueWorker']);