<?php

namespace App\Http\Middleware;

use Closure;

class BasicAuthenticationMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $header = $request->header('api-key');
        if(!$header || $header != 'b67a4557de33de8ce04e47ff7b7ecf42') {
            return response('Unauthorized',401);
        }
        return $next($request);
    }
}
