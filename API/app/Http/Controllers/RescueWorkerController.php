<?php

namespace App\Http\Controllers;

use App\RescueWorker;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class RescueWorkerController extends Controller {

    public function showAllRescueWorkers() {
        return response()->json(RescueWorker::all());
    }

    public function showOneRescueWorker($phone) {
        return response()->json(RescueWorker::find($phone));
    }

    public function createOrUpdateRescueWorker(Request $request) {
        $helpRequestID = $request->input('HelpRequestID');
        $workerName = $request->input('WorkerName');
        $workerPhone = $request->input('WorkerPhone');
        $latitude = $request->input('Latitude');
        $longitude = $request->input('Longitude');
        $rescueWorker = RescueWorker::firstOrNew(['WorkerPhone' => $workerPhone]);
        if($rescueWorker->HelpRequestID == NULL) {
            $rescueWorker->HelpRequestID = $helpRequestID;
        }
        $rescueWorker->WorkerName = $workerName;
        $rescueWorker->WorkerPhone = $workerPhone;
        $rescueWorker->Longitude = $longitude;
        $rescueWorker->Latitude = $latitude;
        $rescueWorker->save();  
        return response()->json("Successfully added");
    }

    public function updateRescueWorkerHelpRequest(Request $request) {
        $workerPhone = $request->input('WorkerPhone');
        $requestID = $request->input('HelpRequestID');
        $rescueWorker = RescueWorker::find($workerPhone);
        if($rescueWorker == NULL) {
            return response("Could not find the specified worker",404);
        }
        $rescueWorker->HelpRequestID = $requestID;
        $rescueWorker->save();
        return response()->json("Successfully assigned");
    }

    public function deleteRescueWorker($phone) {
        $rescueWorker = RescueWorker::find($phone);
        if($rescueWorker == NULL) {
            return response("Could not find the specified worker",404);
        }
        $rescueWorker->delete();
        return response("Deleted Succesfully",200);
    }
}