<?php

namespace App\Http\Controllers;

use App\RescueWorker;
use App\HelpRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HelpRequestController extends Controller {

    public function convertCollectionToIST($helpRequests) {
        if(!$helpRequests) {
            return $helpRequests;
        }
        else {
            return $helpRequests->map(function($helpRequests) {
                $date = new \DateTime($helpRequests->Time);
                $helpRequests->Time = $date->add(new \DateInterval('PT5H30M'))->format('h:i A, d M, Y');
                return $helpRequests;
            });
        }
    }

    public function convertToIST($helpRequest) {
        if(!$helpRequest) {
            return $helpRequest;
        }
        else {
            $date = new \DateTime($helpRequest->Time);
            $helpRequest->Time = $date->add(new \DateInterval('PT5H30M'))->format('h:i A, d M, Y');
            return $helpRequest;
        }
    }

    public function showAllHelpRequests() {
        return response()->json($this->convertCollectionToIST(HelpRequest::all()));
    }

    public function showOneHelpRequest($id) {
        return response()->json($this->convertToIST(HelpRequest::find($id)));
    }

    /**
     * Create a new help request instance.
     *
     * @param  Request  $request
     * @return Response
     */

    public function addHelpRequest(Request $request) {
        $helpRequest = HelpRequest::create($request->all());
        return response()->json($helpRequest,201);
    }

    public function completeHelpRequest(Request $request) {
        $helpRequestID = $request->input('RequestID');
        $workerPhone = $request->input('WorkerNumber');
        $rescueWorker = RescueWorker::find($workerPhone);
        if($rescueWorker == NULL) {
            return response("Could not find rescue worker", 404);
        }
        $helpRequest = HelpRequest::find($helpRequestID);
        if($helpRequest == NULL) {
            return response("Could not find help request",404);
        }        
        $rescueWorker->HelpRequestID = NULL;
        $helpRequest->Status = 0;

        DB::beginTransaction();
        try{
            $rescueWorker->save();
            $helpRequest->save();
            DB::commit();
            return response("Completed request #$helpRequestID",200);        
        }
        catch(\Exception $e) {
            DB::rollback();
            return response($e);
        }
    }
}