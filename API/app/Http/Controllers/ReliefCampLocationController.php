<?php

namespace App\Http\Controllers;

use App\ReliefCampLocation;
use Illuminate\Http\Request;

class ReliefCampLocationController extends Controller {

    public function showAllReliefCampLocations() {
        return response()->json(ReliefCampLocation::all());
    }

    public function showOneReliefCampLocation($id) {
        return response()->json(ReliefCampLocation::find($id));
    }
}