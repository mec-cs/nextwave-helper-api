<?php

namespace App\Http\Controllers;

use App\Announcement;
use Illuminate\Http\Request;

class AnnouncementController extends Controller {

    public function convertCollectionToIST($announcements) {
        if(!$announcements) {
            return $announcements;
        }
        else {
            return $announcements->map(function($announcements) {
                $date = new \DateTime($announcements->Time);
                $announcements->Time = $date->add(new \DateInterval('PT5H30M'))->format('h:i A, d M, Y');
                return $announcements;
            });
        }
    }

    public function convertToIST($announcement) {
        if(!$announcement) {
            return $announcement;
        }
        else {
            $date = new \DateTime($announcement->Time);
            $announcement->Time = $date->add(new \DateInterval('PT5H30M'))->format('h:i A, d M, Y');
            return $announcement;
        }
    }

    public function showAllAnnouncements() {
        return response()->json($this->convertCollectionToIST(Announcement::all()));
    }

    public function showOneAnnouncement($id) { 
        return response()->json($this->convertToIST(Announcement::find($id)));
    }

    public function showLatestAnnouncement() {
        return response()->json($this->convertToIST(Announcement::orderBy('AnnouncementID','desc')->first()));
    }
}