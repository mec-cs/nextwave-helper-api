<?php

namespace App\Http\Controllers;

use App\MissingPerson;
use Illuminate\Http\Request;

class MissingPersonController extends Controller {

    public function convertCollectionToIST($missingPersons) {
        if(!$missingPersons) {
            return $missingPersons;
        }
        else {
            return $missingPersons->map(function($missingPersons) {
                $date = new \DateTime($missingPersons->Time);
                $missingPersons->Time = $date->add(new \DateInterval('PT5H30M'))->format('h:i A, d M, Y');
                return $missingPersons;
            });
        }
    }

    public function convertToIST($missingPerson) {
        if(!$missingPerson) {
            return $missingPerson;
        }
        else {
            $date = new \DateTime($missingPerson->Time);
            $missingPerson->Time = $date->add(new \DateInterval('PT5H30M'))->format('h:i A, d M, Y');
            return $missingPerson;
        }
    }

    public function showAllMissingPersons() {
        return response()->json($this->convertCollectionToIST(MissingPerson::all()));
    }

    public function showOneMissingPerson($id) {
        return response()->json($this->convertToIST(MissingPerson::find($id)));
    }

    /**
     * Create a new missing person instance.
     *
     * @param  Request  $request
     * @return Response
     */

    public function addMissingPerson(Request $request) {
        $missingPerson = MissingPerson::create($request->all());
        return response()->json($missingPerson,201);
    }
}