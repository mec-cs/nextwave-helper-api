<?php

namespace App\Http\Controllers;

use App\AffectedLocation;
use Illuminate\Http\Request;

class AffectedLocationController extends Controller {

    public function convertToGeoJSON($affectedLocations) {
        $affectedLocations = $affectedLocations->map(function($affectedLocation) {
            $affectedLocation->Feature = json_decode($affectedLocation->Feature);
            return $affectedLocation;
        });
        $geoJson = new \stdClass();
        $geoJson->type = "FeatureCollection";
        $geoJson->features = array();
        foreach($affectedLocations as $affectedLocation) {
            array_push($geoJson->features, $affectedLocation->Feature);
        }
        return $geoJson;
    }

    public function showAllAffected() {
        $affectedLocations = AffectedLocation::all();
        $affectedLocations = $this->convertToGeoJSON($affectedLocations);
        return response()->json($affectedLocations);
    }

    public function addOneAffected(Request $request) {
        $markID = $request->input('id');
        $feature = $request->getContent();
        $location = AffectedLocation::create(['MarkID'=>$markID, 'Feature'=>$feature]);
        $location->save();
        return response("Inserted succesfully", 201);
    }

    public function deleteOneAffected($id) {
        $affectedLocation = AffectedLocation::find($id);
        if($affectedLocation == null) 
            return response("Could not find the specified resource on this server",404);
        $affectedLocation->delete();
        return response("Deleted Succesfully",200);
    }
}