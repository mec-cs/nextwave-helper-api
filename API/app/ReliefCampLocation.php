<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReliefCampLocation extends Model
{

    protected $table = 'reliefcamplocations';
    protected $primaryKey = 'CampID';
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'CampID','Phone','Name','Longitude','Latitude','Address'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];
}