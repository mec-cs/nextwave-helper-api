<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RescueWorker extends Model
{
    protected $table = 'rescueworkers';
    protected $primaryKey = 'WorkerPhone';
    public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'HelpRequestID', 'WorkerName','WorkerPhone','Latitude','Longitude'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];
}