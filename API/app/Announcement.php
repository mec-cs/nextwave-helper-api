<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Announcement extends Model
{

    protected $table = 'announcements';
    protected $primaryKey = 'AnnouncementID';
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'AnnouncementID','Time','Description','ImportanceLevel'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];
}