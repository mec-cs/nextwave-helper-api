<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MissingPerson extends Model
{
    protected $table = 'missingpersons';
    protected $primaryKey = 'PersonID';
    public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'PersonID','Name','Sex','Age','Phone','Location','Missing','Time'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];
}