<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AffectedLocation extends Model
{

    protected $table = 'affectedlocations';
    protected $primaryKey = 'MarkID';
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'MarkID','Feature'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];
}