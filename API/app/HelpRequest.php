<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HelpRequest extends Model
{

    protected $table = 'helprequests';
    protected $primaryKey = 'RequestID';
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'RequestID','Time','Name','Phone','Latitude','Longitude','Details','Location','Needs','Status'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [];
}